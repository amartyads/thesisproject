﻿namespace ThesisProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxSearchKeyword = new System.Windows.Forms.TextBox();
            this.buttonUploadFile = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.numericUpDownNoResults = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxShowFileName = new System.Windows.Forms.TextBox();
            this.listBoxResults = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonOpenResult = new System.Windows.Forms.Button();
            this.buttonClearResults = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoResults)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter search text:";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBoxSearchKeyword
            // 
            this.textBoxSearchKeyword.Location = new System.Drawing.Point(108, 6);
            this.textBoxSearchKeyword.Name = "textBoxSearchKeyword";
            this.textBoxSearchKeyword.Size = new System.Drawing.Size(114, 20);
            this.textBoxSearchKeyword.TabIndex = 1;
            // 
            // buttonUploadFile
            // 
            this.buttonUploadFile.Location = new System.Drawing.Point(12, 35);
            this.buttonUploadFile.Name = "buttonUploadFile";
            this.buttonUploadFile.Size = new System.Drawing.Size(210, 23);
            this.buttonUploadFile.TabIndex = 2;
            this.buttonUploadFile.Text = "Upload file";
            this.buttonUploadFile.UseVisualStyleBackColor = true;
            this.buttonUploadFile.Click += new System.EventHandler(this.buttonUploadFile_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Uploaded file name:";
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(228, 6);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(95, 78);
            this.buttonSearch.TabIndex = 4;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // numericUpDownNoResults
            // 
            this.numericUpDownNoResults.Location = new System.Drawing.Point(119, 90);
            this.numericUpDownNoResults.Name = "numericUpDownNoResults";
            this.numericUpDownNoResults.Size = new System.Drawing.Size(204, 20);
            this.numericUpDownNoResults.TabIndex = 5;
            this.numericUpDownNoResults.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Number of results:";
            // 
            // textBoxShowFileName
            // 
            this.textBoxShowFileName.Location = new System.Drawing.Point(119, 64);
            this.textBoxShowFileName.Name = "textBoxShowFileName";
            this.textBoxShowFileName.Size = new System.Drawing.Size(103, 20);
            this.textBoxShowFileName.TabIndex = 7;
            // 
            // listBoxResults
            // 
            this.listBoxResults.FormattingEnabled = true;
            this.listBoxResults.Location = new System.Drawing.Point(15, 138);
            this.listBoxResults.Name = "listBoxResults";
            this.listBoxResults.Size = new System.Drawing.Size(308, 199);
            this.listBoxResults.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Results";
            // 
            // buttonOpenResult
            // 
            this.buttonOpenResult.Location = new System.Drawing.Point(12, 343);
            this.buttonOpenResult.Name = "buttonOpenResult";
            this.buttonOpenResult.Size = new System.Drawing.Size(134, 23);
            this.buttonOpenResult.TabIndex = 10;
            this.buttonOpenResult.Text = "Open selected...";
            this.buttonOpenResult.UseVisualStyleBackColor = true;
            this.buttonOpenResult.Click += new System.EventHandler(this.buttonOpenResult_Click);
            // 
            // buttonClearResults
            // 
            this.buttonClearResults.Location = new System.Drawing.Point(152, 344);
            this.buttonClearResults.Name = "buttonClearResults";
            this.buttonClearResults.Size = new System.Drawing.Size(171, 23);
            this.buttonClearResults.TabIndex = 11;
            this.buttonClearResults.Text = "Clear";
            this.buttonClearResults.UseVisualStyleBackColor = true;
            this.buttonClearResults.Click += new System.EventHandler(this.buttonClearResults_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 379);
            this.Controls.Add(this.buttonClearResults);
            this.Controls.Add(this.buttonOpenResult);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listBoxResults);
            this.Controls.Add(this.textBoxShowFileName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numericUpDownNoResults);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonUploadFile);
            this.Controls.Add(this.textBoxSearchKeyword);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNoResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxSearchKeyword;
        private System.Windows.Forms.Button buttonUploadFile;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.NumericUpDown numericUpDownNoResults;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxShowFileName;
        private System.Windows.Forms.ListBox listBoxResults;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonOpenResult;
        private System.Windows.Forms.Button buttonClearResults;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

