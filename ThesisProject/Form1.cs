﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Diagnostics;
using System.Xml;
using DocumentFormat.OpenXml.Packaging;

namespace ThesisProject
{
    public partial class Form1 : Form
    {
        static IMongoClient _client;
        static IMongoDatabase _database;
        List<BsonDocument> result;
        List<string> files;
        int noRes;
        public Form1()
        {
            InitializeComponent();
            _client = new MongoClient();
            _database = _client.GetDatabase("CBIRtest");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            string query = textBoxSearchKeyword.Text;
            result = null;
            noRes = Convert.ToInt32(numericUpDownNoResults.Value);
            textBoxShowFileName.Text = "";
            searchDBSVG(query);
            searchDBDOCX(query);
            files = new List<string>();
            for (int i = 0; i < Math.Min(noRes, result.Count); i++)
            {
                string temp = result.ElementAt(i).GetElement("filename").ToString();
                temp = temp.Substring(temp.IndexOf('=') + 1);
                temp += ".";
                string temp2 = result.ElementAt(i).GetElement("filetype").ToString();
                temp2 = temp2.Substring(temp2.IndexOf('=') + 1);
                temp += temp2;
                files.Add(temp);
            }
            if (files == null || files.Count == 0) files.Add("No search results");
            listBoxResults.DataSource = files;
        }

        private void searchDBSVG(string query)
        {
            searchDBSVG(query.Split(' '), query.Split(' '), query.Split(' '));
        }
        private void searchDBDOCX(string query)
        {
            searchDBDOCX(query, query, query, query, query, query);
        }

        private void searchDBDOCX(string title, string author, string subject, string categories, string keywords, string description)
        {
            var collect = _database.GetCollection<BsonDocument>("docxfiles");
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.Eq("title", "init");

            filter = builder.Regex("title", new BsonRegularExpression("/" + title + "/"));
            if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
            else result = collect.Find(filter).ToList();
            if (result != null && result.Count >= noRes) return;

            filter = builder.Regex("author", new BsonRegularExpression("/" + author + "/"));
            if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
            else result = collect.Find(filter).ToList();
            if (result != null && result.Count >= noRes) return;

            filter = builder.Regex("subject", new BsonRegularExpression("/" + subject + "/"));
            if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
            else result = collect.Find(filter).ToList();
            if (result != null && result.Count >= noRes) return;

            filter = builder.Regex("categories", new BsonRegularExpression("/" + categories + "/"));
            if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
            else result = collect.Find(filter).ToList();
            if (result != null && result.Count >= noRes) return;

            filter = builder.Regex("keywords", new BsonRegularExpression("/" + keywords + "/"));
            if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
            else result = collect.Find(filter).ToList();
            if (result != null && result.Count >= noRes) return;

            filter = builder.Regex("description", new BsonRegularExpression("/" + description + "/"));
            if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
            else result = collect.Find(filter).ToList();

        }

        private void searchDBSVG(string[] text1, string[] text2, string[] text3)
        {
            var collect = _database.GetCollection<BsonDocument>("svgfiles");
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.Eq("title", "init");
            foreach (string temp in text1)
            {
                filter = builder.Regex("title", new BsonRegularExpression("/" + temp + "/"));
                if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
                else result = collect.Find(filter).ToList();
            }
            if (result == null || result.Count < noRes)
            {
                foreach (string temp in text2)
                {
                    filter = builder.Regex("desc", new BsonRegularExpression("/" + text2 + "/"));
                    try
                    {
                        List<BsonDocument> result2 = collect.Find(filter).ToList();
                        if (result != null) result = result.Union(result2).ToList();
                        else result = result2.ToList();
                    }
                    catch (Exception) { }
                }
                if (result != null && result.Count > noRes) { }
                else
                {
                    foreach (string temp in text3)
                    {
                        if (temp != "")
                        {
                            filter = builder.Regex("text", new BsonRegularExpression("/" + temp + "/"));
                            try
                            {
                                if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
                                else result = collect.Find(filter).ToList();
                            }
                            catch (Exception) { }
                        }
                    }
                }
            }
        }

        private void buttonOpenResult_Click(object sender, EventArgs e)
        {
            int sel = listBoxResults.SelectedIndex;
            string ss = listBoxResults.SelectedItem.ToString();
            if (ss.Equals("No search results")) { }
            else
            {
                string temp = result.ElementAt(sel).GetElement("filepath").ToString();
                temp = temp.Substring(temp.IndexOf('=') + 1);
                Process proc = new Process();
                proc.StartInfo.FileName = temp;
                proc.StartInfo.UseShellExecute = true;
                proc.Start();
            }
        }

        private void buttonClearResults_Click(object sender, EventArgs e)
        {
            files = new List<string>();
            listBoxResults.DataSource = files;
        }

        private void buttonUploadFile_Click(object sender, EventArgs e)
        {
            DialogResult res = openFileDialog1.ShowDialog();
            if (res == DialogResult.OK)
            {
                textBoxSearchKeyword.Text = "";
                string file = openFileDialog1.FileName;
                string ext = file.Substring(file.LastIndexOf('.') + 1);
                textBoxShowFileName.Text = file.Substring(file.IndexOf("=") + 1);
                searchByContent(file, ext);
            }
        }

        private void searchByContent(string file, string ext)
        {
            result = null;
            noRes = Convert.ToInt32(numericUpDownNoResults.Value);
            if (ext == "svg")
            {
                string[] title, desc, text;
                XmlDocument pic = new XmlDocument();
                string loc = file;
                pic.Load(loc);
                //XmlNodeList node = pic.ChildNodes;
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(pic.NameTable);
                string strNS = pic.DocumentElement.NamespaceURI;
                nsMgr.AddNamespace("ns", strNS);
                XmlNode node = pic.SelectSingleNode("/ns:svg/ns:desc", nsMgr);
                desc = node.InnerText.Split(' ');
                node = pic.SelectSingleNode("/ns:svg/ns:title", nsMgr);
                title = node.InnerText.Split(' ');
                text = getTextFromSVG(pic, nsMgr).Split(' ');
                for (int i = 0; i < title.Length; i++)
                {
                    title[i] = title[i].ToLower();
                }
                for (int i = 0; i < desc.Length; i++)
                {
                    desc[i] = desc[i].ToLower();
                }
                for (int i = 0; i < text.Length; i++)
                {
                    text[i] = text[i].ToLower();
                }
                //query
                searchDBSVG(title, desc, text);
            }
            else if (ext == "docx")
            {
                using (WordprocessingDocument filet = WordprocessingDocument.Open(file, false))
                {
                    string title;
                    try { title = filet.PackageProperties.Title.ToLower(); }
                    catch { title = ""; }

                    string author;
                    try { author = filet.PackageProperties.Creator.ToLower(); }
                    catch { author = ""; }

                    string subject;
                    try { subject = filet.PackageProperties.Subject.ToLower(); }
                    catch { subject = ""; }

                    string category;
                    try { category = filet.PackageProperties.Category.ToLower(); }
                    catch { category = ""; }

                    string keywords;
                    try { keywords = filet.PackageProperties.Keywords.ToLower(); }
                    catch { keywords = ""; }

                    string description;
                    try { description = filet.PackageProperties.Description.ToLower(); }
                    catch { description = ""; }

                    //DateTime? created;
                    //try { created = file.PackageProperties.Created; }
                    //catch { created = null; }

                    //DateTime? modified;
                    //try { modified = file.PackageProperties.Modified; }
                    //catch { modified = null; }
                    searchDBDOCX(title, author, subject, category, keywords, description);
                }
            }
            files = new List<string>();
            for (int i = 0; i < Math.Min(noRes, result.Count); i++)
            {
                string temp = result.ElementAt(i).GetElement("filename").ToString();
                temp = temp.Substring(temp.IndexOf('=') + 1);
                temp += ".";
                string temp2 = result.ElementAt(i).GetElement("filetype").ToString();
                temp2 = temp2.Substring(temp2.IndexOf('=') + 1);
                temp += temp2;
                files.Add(temp);
            }
            if (files == null || files.Count == 0) files.Add("No search results");
            listBoxResults.DataSource = files;
        }
        private string getTextFromSVG(XmlDocument pic, XmlNamespaceManager nsMgr)
        {
            string allText = "";
            string gs = "";
            bool exit = false;
            while (!exit)
            {
                XmlNodeList toxts = pic.SelectNodes("/ns:svg" + gs + "/ns:text", nsMgr);
                foreach (XmlNode textnode in toxts)
                {
                    allText += textnode.InnerText + " ";
                }
                XmlNodeList nodes = pic.SelectNodes("/ns:svg" + gs + "/ns:g", nsMgr);
                if (nodes.Count == 0)
                    exit = true;
                else
                    gs += "/ns:g";
            }
            StringBuilder sb = new StringBuilder();
            string[] parts = allText.Split(new char[] { ' ', '\n', '\t', '\r', '\f', '\v', '\\' }, StringSplitOptions.RemoveEmptyEntries);
            int size = parts.Length;
            for (int i = 0; i < size; i++)
                if (parts[i].Length > 2)
                    sb.AppendFormat("{0} ", parts[i]);
            allText = sb.ToString().Trim();
            return allText;
        }
    }
}
